package com.nfyg.loading_master;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by joey.huang on 2017/6/19.
 */

public class DotLoading extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dot);
    }
}
